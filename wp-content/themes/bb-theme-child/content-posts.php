<?php

$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
$thumb_size   = FLTheme::get_setting('fl-posts-thumb-size');
?>
<?php do_action('fl_before_post'); ?>
<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/BlogPosting">

	<?php if(has_post_thumbnail() && !empty($show_thumbs)) : ?>
		<?php if($show_thumbs == 'above-title') : ?>
		<div class="fl-post-thumb">
			<?php the_post_thumbnail('newspage-thumb', array('itemprop' => 'image')); ?>
		</div>
		<?php endif; ?>
	<?php endif; ?>

	<?php if(has_post_thumbnail() && !empty($show_thumbs)) : ?>
		<?php if($show_thumbs == 'above') : ?>
		<div class="fl-post-thumb">
			<?php the_post_thumbnail('newspage-thumb'); ?>
		</div>
		<?php endif; ?>

		<?php if($show_thumbs == 'beside') : ?>
		<?php if(isMobile()){ ?>	
	<div class="archivepost-loop">
<?php } else { ?>
<div class="row">
	<?php } ?>	
				<div class="fl-post-image-<?php echo $show_thumbs; ?> col-md-4">
					<div class="fl-post-thumb">
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
							<?php the_post_thumbnail('newspage-thumb'); ?>
						</a>
					</div>
				</div>
				<div class="fl-post-content-<?php echo $show_thumbs; ?> col-md-8">
		<?php endif; ?>
	<?php endif; ?>

	<?php do_action('fl_before_post_content'); ?>

	<header class="fl-post-header">
	<?php if(isMobile()){ ?>	
		<strong class="postDate"><?php $post_date = get_the_date( 'm-d-Y' ); echo $post_date; ?></strong>
	<?php } ?>	
	<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><h3 class="fl-post-title" itemprop="headline">
			<?php the_title(); ?>			
		</h3>
	</a>
		
		<?php //FLTheme::post_top_meta(); ?>
	</header><!-- .fl-post-header -->

	<div class="fl-post-content clearfix" itemprop="text">

	  	
		<?php

		 the_excerpt();
		 
		wp_link_pages( array(
			'before'         => '<div class="fl-post-page-nav">' . _x( 'Pages:', 'Text before page links on paginated post.', 'fl-automator' ),
			'after'          => '</div>',
			'next_or_number' => 'number'
		) );

		?>
	</div><!-- .fl-post-content -->
	<?php if(!isMobile()){ ?>	
	<strong class="postDate"><?php $post_date = get_the_date( 'm-d-Y' ); echo $post_date; ?></strong>
    <?php } ?>
	<?php if(has_post_thumbnail() && $show_thumbs == 'beside') : ?>
		</div>
	</div>
	<?php endif; ?>

	<?php FLTheme::post_bottom_meta(); ?>
	<?php FLTheme::post_navigation(); ?>
	<?php do_action('fl_after_post_content'); ?>

</article>
<?php comments_template(); ?>

<?php do_action('fl_after_post'); ?>

<!-- .fl-post -->
