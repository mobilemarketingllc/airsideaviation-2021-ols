<?php
/**
 * Custom Post Type Recent Posts widget class
 *
 * @since 1.0.0
 * @package Custom Post Type Widgets
 */

/**
 * Core class WP_Custom_Post_Type_Widgets_Recent_Posts
 *
 * @since 1.0.0
 */
class WP_Custom_Post_Type_Widgets_Recent_Posts extends WP_Widget {

	/**
	 * Sets up a new widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'widget_recent_entries',
			'description'                 => __( 'Your site&#8217;s most recent custom Posts.', 'custom-post-type-widgets' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'custom-post-type-recent-posts', __( 'Recent Posts (Custom Post Type)', 'custom-post-type-widgets' ), $widget_ops );
		$this->alt_option_name = 'widget_custom_post_type_recent_posts';
	}

	/**
	 * Outputs the content for the widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts', 'custom-post-type-widgets' ) : $instance['title'], $instance, $this->id_base );
		$posttype = ! empty( $instance['posttype'] ) ? $instance['posttype'] : 'post';
		if ( empty( $instance['number'] ) || ! $number = absint( $instance['number'] ) ) {
			$number = 5;
		}
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		$post_types = get_post_types( array( 'public' => true ), 'objects' );

		if ( array_key_exists( $posttype, (array) $post_types ) ) {
			$r = new WP_Query( apply_filters( 'widget_posts_args', array(
				'post_type' => $posttype,
				'posts_per_page' => $number,
				'no_found_rows' => true,
				'post_status' => 'publish',
				'ignore_sticky_posts' => true,
			) ) );

			if ( $r->have_posts() ) : ?>
				<?php echo $args['before_widget']; ?>
				<?php if ( $title ) {
					//echo $args['before_title'] . $title . $args['after_title'];
				} ?>
	
	<?php while ( $r->have_posts() ) : $r->the_post(); $title = get_the_title(); $price = get_field('price');?>
	
    <div class="fl-post-grid-post airlist" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">

	<a itemprop="url" href="<?php the_permalink(); ?>" title="">
            <div class="inventory-img">
			 <?php if( get_field('display_reduced_price_text') == 'show' ): ?><span class="reducedtext">
				<?php if( get_field('enter_display_text') != '' ){echo the_field('enter_display_text');} else { echo 'Reduced Price!';} ?></span><?php endif; ?>
				
			 <?php if( get_field('sold') ){ ?><span class="soldtext">Sold!</span><?php } ?>   
               <img src="<?php the_post_thumbnail_url(); ?>" style="width:100%;"/>
               
            </div>
        
		
        <div class="fl-post-grid-text product-grid">
 
            <div class="product-grid-top" style="background-color:#fff;margin-top:-10px;padding-top:10px;">
					<h4 style="text-align: center;" itemprop="name"><?php  the_title(); ?></h4>				
					<p style="text-align: center; font-size: 13px; color: #021420;"><?php if( get_field('year_built') ): ?>Year <strong><?php the_field('year_built'); ?></strong><?php endif; ?></p>
					<p class="customPrice" style="text-align: center; color: #f89621;"><strong><?php if( get_field('price') ): ?><?php the_field('price'); ?><?php endif; ?></strong></p>	
				
					<div class="home-divider"></div>
					<div class="icon-sep jeticons">
                      <?php if( get_field('max_renge') ): ?>
                        <div class="hours-icon">
                                    <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/range.png" alt="" ><br>
                                    <?php the_field('max_renge') ;?><br> Max Range</div>
                         <?php endif; ?>
                         <?php /*if( get_field('max_cruise_speed') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/max_speed.png" alt="" ><br>
                            <?php the_field('max_cruise_speed') ;?> <br>Max Cruise Speed</div>
                        <?php endif;*/ ?>
						<?php if( get_field('hours') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/08/hours.png" alt="" ><br>
                            <?php the_field('hours') ;?> <br>Hours</div>
                        <?php endif; ?>	
                        <?php if( get_field('passengers') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/owners.png" alt="" ><br>
                 <?php if( get_field('passengers')!=1 ){ echo get_field('passengers').' Passengers';}else{ echo get_field('passengers').' Passenger';}?></div>
                        <?php endif; ?>
                 </div>

			</div>

		</div>
		</a>

		<?php $jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>get_the_post_thumbnail_url(get_the_ID() , 'thumbnail' ),'description'=>$title,'sku'=> get_the_ID() ,'mpn'=>get_the_ID(),'brand'=>array('@type'=>'thing','name'=>$title), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'0','priceValidUntil'=>'')); ?>
	<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>
	</div>
				<?php endwhile; ?>
				
				

				<?php echo $args['after_widget']; ?>
				<?php
				wp_reset_postdata();
			endif;
		}
	}

	/**
	 * Handles updating settings for the current Archives widget instance.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via form() method.
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = empty( $new_instance['title'] ) ? '' : sanitize_text_field( $new_instance['title'] );
		$instance['posttype']  = wp_strip_all_tags( $new_instance['posttype'] );
		$instance['number']    = absint( $new_instance['number'] );
		$instance['show_date'] = ! empty( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;

		return $instance;
	}

	/**
	 * Outputs the settings form for the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? $instance['title'] : '';
		$posttype  = isset( $instance['posttype'] ) ? $instance['posttype'] : 'post';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = ! empty( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>"><?php esc_html_e( 'Title:', 'custom-post-type-widgets' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" name="<?php echo $this->get_field_name( 'title' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

		<?php
		printf(
			'<p><label for="%1$s">%2$s</label>' .
			'<select class="widefat" id="%1$s" name="%3$s">',
			/* phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped */
			$this->get_field_id( 'posttype' ),
			/* phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped */
			__( 'Post Type:', 'custom-post-type-widgets' ),
			/* phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped */
			$this->get_field_name( 'posttype' )
		);

		printf(
			'<option value="%s"%s>%s</option>',
			esc_attr( 'any' ),
			selected( 'any', $posttype, false ),
			esc_html__( 'All', 'custom-post-type-widgets' )
		);

		$post_types = get_post_types( array( 'public' => true ), 'objects' );

		foreach ( $post_types as $post_type => $value ) {
			if ( 'attachment' === $post_type ) {
				continue;
			}

			printf(
				'<option value="%s"%s>%s</option>',
				esc_attr( $post_type ),
				selected( $post_type, $posttype, false ),
				esc_html__( $value->label, 'custom-post-type-widgets' )
			);

		}
		echo '</select></p>';
		?>

		<p><label for="<?php echo $this->get_field_id( 'number' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>"><?php esc_html_e( 'Number of posts to show:', 'custom-post-type-widgets' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" name="<?php echo $this->get_field_name( 'number' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" type="text" value="<?php echo esc_attr( $number ); /* @phpstan-ignore-line */ ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" name="<?php echo $this->get_field_name( 'show_date' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>"><?php esc_html_e( 'Display post date?', 'custom-post-type-widgets' ); ?></label></p>
		<?php
	}
}
