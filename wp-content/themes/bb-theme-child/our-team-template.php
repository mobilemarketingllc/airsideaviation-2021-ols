<?php /* Template Name: Our Team Template */ ?>



<?php get_header(); ?>

<script>
function get_productid(thisName,rid)
{
    if(thisName!='')
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
				
				jQuery('.arrowShow').hide();
				jQuery('.collapseInfo').hide();
				jQuery("#expandInfo"+rid).slideDown(800);
				jQuery('.arrow-show'+thisName).show();
				//document.getElementsByClassName('collapseInfo').style.display = 'none';
				//document.getElementById("expandInfo"+rid).style.display = 'block';
                document.getElementById("expandInfo"+rid).innerHTML = this.responseText;
				//jQuery("expandInfo"+rid).show();
            }
        };
     xhttp.open("GET", "<?php echo get_stylesheet_directory_uri();?>/proajax.php?y="+thisName, true);
     xhttp.send();
    }
}
function toggleclose()
	{
		
		
		jQuery('.showBarInfo').slideUp();
		jQuery('.pro-colornameActive').removeClass('pro-colornameActive');
		jQuery('.arrowShow').hide();
	
	}
function toggleInfo(thisName)
{
    //jQuery('.showBarInfo').hide();
    //jQuery(thisName).slideToggle( "slow" );
}
	//jQuery(document).ready(function(){
		
  //jQuery('html,body').animate({scrollTop:jQuery(location.hash).offset().top}, 1000);
//});
</script>
<?php 
$team_page_label = get_field('team_page_label');
$team_page_background = get_field('team_page_background');
$team_page_description = get_field('team_page_description');
if($team_page_background=="Color")
{
    $bgsetting = get_field('apply_color');
}
else
{
    $bgsetting = "url(".get_field('apply_image').")";
}
?>
<div class="fl-page-content" itemprop="mainContentOfPage">  		
            <div class="fl-content-full container">
	            <div class="row">
					<div class="col-md-1 col-xs-12"></div>
					<div class="col-md-10 col-xs-12">
						<div class="fl-rich-text">
					   		<h2 class="headerOfPage" style="text-align: center;background:<?php echo $bgsetting; ?> ;"><strong><?php echo $team_page_label; ?></strong></h2>
							<div class="teamdescription" style="text-align: center;"><?php echo $team_page_description; ?></div>
						</div>
					</div>
					<div class="col-md-1 col-xs-12"></div>
                 </div>
 
    <div class="container product-detail" style="margin-top:30px;">
	<div class="row">
    <?php  $i=0;  $j=1; ?>          
    <?php $args = array('post_type' => 'team', 'posts_per_page' =>'-1'); ?>
    <?php $loop = new WP_Query($args); ?>
    <?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post();  $i++;?>
            <div class="col-md-4 col-xs-12 teamMember" id="expand<?php echo $post->ID; ?>" >
               
               <a  href="javascript:void(0)" class="delAction<?php echo $post->ID; ?> imgfixht" id="<?php echo $j; ?>" onclick="get_productid(this.name,'<?php echo $j;?>');" name="<?php echo $post->ID; ?>" > 
               
               <?php 
                        $teamimage = get_field('profile_image');
                        $size = 'medium'; // (thumbnail, medium, large, full or custom size)
                        $thumb = $teamimage['sizes'][ $size ];
                        
                ?>
               
               <img src="<?php if($thumb!='') {echo $thumb;} else{ echo '/wp-content/uploads/2018/06/no-team-image.png';} ?>"/>
              </a>
            
                 <h3 class="team-name"> <?php the_title(); ?></h3>
                  <span class="team-job"><?php  the_field('job_title'); ?></span>
                  <p class="arrow-show<?php echo $post->ID; ?> arrowShow" style="display:none;">                  
			            <img src="/wp-content/uploads/2018/06/if_arrow_sans_up_103304.png" width="36" height="36"/>
		          </p>
            </div>

            
	<?php 					 
							 
		if(isMobile()){
		if($i%1 == 0)  { ?><div id="expandInfo<?php echo $j;?>" class="collapseInfo"></div><?php $j++; }  
		}
		else {
			if($i%3 == 0) { ?><div id="expandInfo<?php echo $j;?>" class="collapseInfo"></div><?php $j++; } 
		}							 
	?>							 
								
       <?php endwhile; ?>
        <?php else: ?>
                 <h1>No posts here!</h1>
        <?php endif; ?>
        <div id="expandInfo<?php echo $j;?>" class="collapseInfo" style="clear:both;"></div>
        <?php wp_reset_postdata(); ?>

	</div>
</div>
</div>
</div>
<?php get_footer(); ?>