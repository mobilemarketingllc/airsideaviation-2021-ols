<header class="fl-page-header fl-page-header-primary<?php FLTheme::header_classes(); ?>"<?php FLTheme::header_data_attrs(); ?> itemscope="itemscope" itemtype="https://schema.org/WPHeader">
	<div class="fl-page-header-wrap">
		<div class="fl-page-header-container container">
			<div class="fl-page-header-row row displayOnlyDesktop">
				<div class="col-md-3 col-sm-12 fl-page-header-logo-col">
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="https://schema.org/Organization">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><?php FLTheme::logo(); ?></a>
					</div>
				</div>
				<div class="fl-page-nav-col col-md-9 col-sm-12">
					<div class="fl-page-nav-wrap">
						<nav class="fl-page-nav fl-nav navbar navbar-default" role="navigation" aria-label="<?php echo esc_attr( FLTheme::get_nav_locations( 'header' ) ); ?>" itemscope="itemscope" itemtype="https://schema.org/SiteNavigationElement">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".fl-page-nav-collapse">
								<span><?php FLTheme::nav_toggle_text(); ?></span>
							</button>
							<div class="fl-page-nav-collapse collapse navbar-collapse">
								<?php

								FLTheme::nav_search();

								wp_nav_menu(array(
									'theme_location' => 'header',
									'items_wrap' => '<ul id="%1$s" class="nav navbar-nav navbar-right %2$s">%3$s</ul>',
									'container' => false,
									'fallback_cb' => 'FLTheme::nav_menu_fallback',
								));

								?>
							</div>
						</nav>
					</div>
				</div>
			</div>
			
			
			<div class="fl-page-header-row row displayOnlyMobile">
				<div class="  fl-page-header-search-col">
						<div class="fl-page-nav-wrap">
						<nav class="fl-page-nav fl-nav navbar navbar-default" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".fl-page-nav-collapse"><span class="fa fa-bars"></span>
							</button>
							<div class="fl-page-nav-collapse collapse navbar-collapse">
						 
								<?php
								
								wp_nav_menu(array(
									'theme_location' => 'mobile-menu',
									'items_wrap' => '<ul id="%1$s" class="nav mobileMenu navbar-nav navbar-right %2$s">%3$s</ul>',
									'container' => false,
									'fallback_cb' => 'FLTheme::nav_menu_fallback'
								));
								?>
							</div>
						</nav>
					</div>
				</div>
				<div class="  fl-page-header-logo-col">
					<div class="fl-page-header-logo" itemscope="itemscope" itemtype="http://schema.org/Organization">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" itemprop="url"><?php FLTheme::logo(); ?></a>
					</div>
				</div>
				<div class="fl-page-nav-col  home">
			<a href="/contact-us/" class="afl-button">Contact Us</a>
				</div>
			</div>
			
			
		</div>
	</div>
</header><!-- .fl-page-header -->

<script>
	jQuery('.displayOnlyMobile .navbar-toggle').click(function () {
		jQuery('body').toggleClass('mobileNavSlide');
	})


	jQuery('.mobileMenu > li.menu-item  a').click(function (event) {
		jQuery('.mobileMenu > li.menu-item > .sub-menu .sub-menu').hide();
		jQuery(this).siblings('ul.sub-menu').slideToggle();
	})

</script>
